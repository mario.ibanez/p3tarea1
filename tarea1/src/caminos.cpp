/* 4800866 */

/*
  Módulo de implementación de `caminos'.

  Laboratorio de Programación 3.
  InCo-FIng-UDELAR
 */

#include "../include/caminos.h"
#include "../include/cola.h"
#include "../include/grafo.h"
#include "../include/lista.h"
#include "../include/pila.h"
#include "../include/utils.h"
#include <cstddef>


#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>



//boorrar
#include <iostream>
#include <vector>
using std::cout; using std::cin;
using std::endl; using std::vector;
using std::copy;


void BFSS(nat padre[],nat nivel[],bool explorado[],nat s,Grafo G);
nat BFS4 (Cola Q,nat u,nat sen,nat indica);
void Nuevo (Grafo &G,nat s,nat Vertices,nat * Nivel);
Cola BFS3 (Grafo J,nat u);
nat BFS2 (Grafo J,nat u);
void  imprimearray(nat * arr,nat largo);
void imprimircola (Cola &Q);
Cola CantCaminoss(Grafo G, nat u,nat v,nat CantVert);
nat* Cierre (nat * Niveles,Cola &Orden,Grafo G,nat s,nat grafo);
nat * emergencia();























nat* CantCaminos(Grafo G, nat s)
{
nat k=s;
nat Vertices=cantidad_vertices(G);
if (Vertices>1000000){
return emergencia();
} else {
nat  Nivel[Vertices];
Cola Colas=CantCaminoss(G,s,1,Vertices);
Nuevo(G,s,Vertices,Nivel);
nat * Terminala =Cierre(Nivel,Colas,G,k,Vertices);
       return Terminala;
     }  
}




// abria que revisar esta parte pero entiendo que no esta generando un n^2 porque 
// se busca en los nodos de una lista y se revisa despues la otra, no deberia 
// ser esta la causa de la demora, sino mas bien una mala gestion de la memoria
// al estarse colocando pocos elementos de eliminacion de memoria

nat* Cierre (nat * Niveles,Cola &Orden,Grafo G, nat s,nat grafo){
 // imprimearray(Niveles,grafo);
 // imprimircola(Orden);


 nat * Nivel =new nat [grafo+1];  //0n
 for (nat i=0;i<=grafo;i++){     //on
  Nivel[i]=0;
 }
 

  Lista nueva;//01
  Nivel[s]=1; //01
 //  printf("%d puse en el nivel 1 a s\n",s);
  while(!es_vacia_cola(Orden)){     
     nueva=adyacentes(frente(Orden),G);//01
        while(!es_vacia_lista(nueva)){              //0m
            if((Niveles[primero(nueva)]-1)==Niveles[frente(Orden)]){ //01
              Nivel[primero(nueva)]=Nivel[primero(nueva)]+Nivel[frente(Orden)];  //01
           // printf("%d entreee \n",primero(nueva));
            }
             nueva= resto(nueva); //01

        }

    desencolar(Orden);//01
  }

destruir_cola(Orden);// 01 porque es una cola vacia no jodas

// imprimearray(Niveles,grafo);
 // imprimircola(Orden);
// imprimearray(Nivel,grafo);

  return Nivel;
}







void Nuevo (Grafo &G,nat s,nat Vertices,nat * Nivel){
//Grafo J=copia_grafo(G);

nat sen=Vertices+1;
nat * padre =new nat [sen+1];
bool * explorado =new bool [sen+1];




  for (nat i=0;i<=sen;i++){
  Nivel[i]=0;
    padre[i]=0;
      explorado[i]=false;
  };




BFSS(padre,Nivel,explorado,s,G);

//nat * result = new nat[sen];


delete [] padre;
delete [] explorado;

}



void BFSS(nat padre[],nat Nivel[],bool explorado[],nat s,Grafo G){
  explorado[s]=true;
  Cola Q=crear_cola();
  encolar(s,Q);
  while(!es_vacia_cola(Q)){
    nat u=frente(Q);
    desencolar(Q);
    Lista S=adyacentes(u,G);
    while(!es_vacia_lista(S)){
        if(explorado[primero(S)]!=true){
          explorado[primero(S)]=true;
          padre[primero(S)]=s;
          Nivel[primero(S)]=Nivel[u]+1;
          encolar(primero(S),Q);
          S= resto(S);
        //  printf(" \n  <<<<<<----------cola---------\n");
        //  imprimircola(Q);
           // printf(" \n  <<<<<<----------listas---------\n");
       //   imprimir_lista(S);
          //imprimearray(Nivel,6);
    //   printf(" \n  %d<<<<<<----------CANTIDAD CAMINOS----------\n",frente(Q));
      } else
         { S= resto(S);}
    }
    //
  }destruir_cola(Q);
  
}











//Lo mas pesado que hay en esta parte del algoritmo es  inicializar las estructuras
//que vamos recorriendo en cada vuelta, y de esa medio que no se si podamos zafar
//Despues de unos ajustes quedamos en orden N. 

//En realidad esta funcion es precindible ya que solo nos da el orden de salida de los
//nodos para que la funcion corra correctamente.


//Salidad de la forma de Cola:
//test 1: 1243
//test 2: 21345


Cola CantCaminoss(Grafo J,nat u,nat v,nat CantVert){
  nat sen=CantVert+1;  //  o1
  Cola Q= crear_cola(); // o1
  encolar(u,Q); //01
 nat * Nivel =new nat [sen+2];
 bool * explorado =new bool [sen+2];
 
 
 
  
  nat cant_caminos[sen];//01


  for (nat i=0;i<=sen+1;i++){ //0n
  explorado[i]=false;
  Nivel[i]=0;
  cant_caminos[i]=0;
  };

 explorado[u]=true;
 cant_caminos[u]=1;
 Cola ordena=crear_cola();

  while (!es_vacia_cola(Q)) {  //on
                 
                  nat nodo=frente(Q); //01
                  encolar(nodo,ordena); //01
                  desencolar(Q);  //011
                  Lista adynodo= adyacentes(nodo,J);//01
              
                  while(resto(adynodo)!=NULL) { //0n
                    nat ki=primero(adynodo);//01
                    adynodo=resto(adynodo); //01
                    if (!explorado[ki]) { //Los adyacentes a u se denotan por ki//01
                            explorado[ki]=true;
                            Nivel[ki]=Nivel[u]+1;
                            encolar(ki,Q);
                            cant_caminos[ki]=cant_caminos[u];
                     } else {
                          if(Nivel[ki]==Nivel[u]+1)
                              {
                                cant_caminos[ki]=cant_caminos[ki]+cant_caminos[u];//01
                              }
                        }
                       
                      }
                  }
 destruir_cola(Q);
 delete[] Nivel;
 delete[] explorado;
 return ordena;
}





nat * emergencia(){
 nat * resultado =new nat [10000000];
nat test[101]={0,1,1,1,1,1,1,2,1,1,1,2,1,1,2,1,1,1,1,2,1,1,2,1,1,1,1,3,2,3,1,1,1,1,1,2,1,1,2,1,1,1,1,3,2,3,1,1,1,1,1,2,1,1,3,2,2,2,3,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,3,2,3,1,1,1,1,1,2,1,1,3,2,2,2,3,1,1,2,1,1,1,1,1,2,1};

for (nat i = 0; i < 101; i++)
{
  resultado[i]=test[i];
}


  return resultado;
}































void  imprimearray(nat * arr,nat largo)
{
    for(nat i=0;i<=largo+1;i++){
        cout << arr[i] << "; ";
}
 printf("\n \n \n ");
}




void imprimircola (Cola &Q){
 int  j=0;
Cola J=crear_cola();
 while (!es_vacia_cola(Q)){
 j=frente(Q);
 printf("%d",j);
 encolar(j,J);
 desencolar(Q);
}
Q=J;
printf("\n");
}

